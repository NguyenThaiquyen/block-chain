const util = require('./utils/index')
const {
  catchRevertWithReason
} = require("./utils/exceptions.js")

contract('LuckyNumber', (accounts) => {
  let luckyNumberInstance
  const { gameAdmin, mainAdmin, user1 } = util.getAccounts(accounts)
  before( async () => {
    const data = await util.initContracts(accounts)
    luckyNumberInstance = data.luckyNumberInstance
  })

  describe('I. Admin setSystemInfoVisibility', () => {
    it('1. fail cause by not admin' , async () => {
      await catchRevertWithReason(luckyNumberInstance.setSystemInfoVisibility(true, { from: user1 }), 'onlyMainAdmin')
    })
    it('2. success' , async () => {
      await luckyNumberInstance.setSystemInfoVisibility(true, { from: gameAdmin })
      const visible = await luckyNumberInstance.systemInfoVisible()
      visible.should.be.true
    })
  })

  describe('II. Admin setMinBet', () => {
    it('1. fail cause by not admin' , async () => {
      await catchRevertWithReason(luckyNumberInstance.setMinBet(0, { from: user1 }), 'onlyMainAdmin')
    })
    it('2. fail cause by input invalid value' , async () => {
      await catchRevertWithReason(luckyNumberInstance.setMinBet(0, { from: gameAdmin }), 'min bet must be > 0')
    })
    it('3. success' , async () => {
      const minBetAmount = 1
      await luckyNumberInstance.setMinBet(minBetAmount, { from: gameAdmin })
      const contractMinBetAmount = await luckyNumberInstance.minBetAmount()
      contractMinBetAmount.should.be.a.bignumber.that.equal(`${minBetAmount}`)

    })
    it('4. should fire event' , async () => {
      const response = await luckyNumberInstance.setMinBet(2, { from: gameAdmin })
      util.listenEvent(response, 'MinBetSet')
    })
  })

  describe('III. Admin setMaxBet', () => {
    it('1. fail cause by not admin' , async () => {
      await catchRevertWithReason(luckyNumberInstance.setMaxBet(0, { from: user1 }), 'onlyMainAdmin')
    })
    it('2. fail cause by input invalid value' , async () => {
      const contractMinBetAmount = await luckyNumberInstance.minBetAmount()
      await catchRevertWithReason(luckyNumberInstance.setMaxBet(contractMinBetAmount, { from: gameAdmin }), 'max bet must be > minBetAmount')
    })
    it('3. success' , async () => {
      const contractMinBetAmount = await luckyNumberInstance.minBetAmount()
      await luckyNumberInstance.setMaxBet(contractMinBetAmount + 1, { from: gameAdmin })
      const contractMaxBetAmount = await luckyNumberInstance.maxBetAmount()
      contractMaxBetAmount.should.be.a.bignumber.that.equal(`${contractMinBetAmount + 1}`)

    })
    it('4. should fire event' , async () => {
      const contractMaxBetAmount = await luckyNumberInstance.maxBetAmount()
      const response = await luckyNumberInstance.setMaxBet(contractMaxBetAmount + 1, { from: gameAdmin })
      util.listenEvent(response, 'MaxBetSet')
    })
  })

  describe('IV. Admin setHighRollCheckpoint', () => {
    it('1. fail cause by not admin' , async () => {
      await catchRevertWithReason(luckyNumberInstance.setHighRollCheckpoint(0, { from: user1 }), 'onlyMainAdmin')
    })
    it('2. fail cause by input invalid value' , async () => {
      await catchRevertWithReason(luckyNumberInstance.setHighRollCheckpoint(0, { from: gameAdmin }), 'high roll must be > 0')
    })
    it('3. success' , async () => {
      const highRollCheckpoint = 1
      await luckyNumberInstance.setHighRollCheckpoint(highRollCheckpoint, { from: gameAdmin })
      const contractHighRollCheckpoint = await luckyNumberInstance.highRollCheckpoint()
      contractHighRollCheckpoint.should.be.a.bignumber.that.equal(`${highRollCheckpoint}`)

    })
    it('4. should fire event' , async () => {
      const response = await luckyNumberInstance.setHighRollCheckpoint(1, { from: gameAdmin })
      util.listenEvent(response, 'HighRollCheckpointSet')
    })
  })

  describe('V. Admin setRareWinCheckpoint', () => {
    it('1. fail cause by not admin' , async () => {
      await catchRevertWithReason(luckyNumberInstance.setRareWinCheckpoint(0, { from: user1 }), 'onlyMainAdmin')
    })
    it('2. fail cause by input invalid value' , async () => {
      await catchRevertWithReason(luckyNumberInstance.setRareWinCheckpoint(99, { from: gameAdmin }), 'rare win is invalid')
      await catchRevertWithReason(luckyNumberInstance.setRareWinCheckpoint(9501, { from: gameAdmin }), 'rare win is invalid')
    })
    it('3. success' , async () => {
      const rareWinCheckpoint = 100
      await luckyNumberInstance.setRareWinCheckpoint(rareWinCheckpoint, { from: gameAdmin })
      const contractHighRollCheckpoint = await luckyNumberInstance.rareWinCheckpoint()
      contractHighRollCheckpoint.should.be.a.bignumber.that.equal(`${rareWinCheckpoint}`)

    })
    it('4. should fire event' , async () => {
      const response = await luckyNumberInstance.setRareWinCheckpoint(100, { from: gameAdmin })
      util.listenEvent(response, 'RareWinCheckpointSet')
    })
  })

  describe('VI. Admin burnDividend', () => {
    // TODO test admin burnDividend
    // it('1. fail cause by not admin' , async () => {
    //   await catchRevertWithReason(luckyNumberInstance.setRareWinCheckpoint(0, { from: user1 }), 'onlyMainAdmin')
    // })
    // it('2. fail cause by input invalid value' , async () => {
    //   await catchRevertWithReason(luckyNumberInstance.setRareWinCheckpoint(99, { from: gameAdmin }), 'rare win is invalid')
    //   await catchRevertWithReason(luckyNumberInstance.setRareWinCheckpoint(9501, { from: gameAdmin }), 'rare win is invalid')
    // })
    // it('3. success' , async () => {
    //   const rareWinCheckpoint = 100
    //   await luckyNumberInstance.setRareWinCheckpoint(rareWinCheckpoint, { from: gameAdmin })
    //   const contractHighRollCheckpoint = await luckyNumberInstance.rareWinCheckpoint()
    //   contractHighRollCheckpoint.should.be.a.bignumber.that.equal(`${rareWinCheckpoint}`)
    //
    // })
    // it('4. should fire event' , async () => {
    //   const response = await luckyNumberInstance.setRareWinCheckpoint(100, { from: gameAdmin })
    //   util.listenEvent(response, 'RareWinCheckpointSet')
    // })
  })

  describe('VII. Admin burnDividend', () => {
    // TODO test admin adminWithdraw
    // it('1. fail cause by not admin' , async () => {
    //   await catchRevertWithReason(luckyNumberInstance.setRareWinCheckpoint(0, { from: user1 }), 'onlyMainAdmin')
    // })
    // it('2. fail cause by input invalid value' , async () => {
    //   await catchRevertWithReason(luckyNumberInstance.setRareWinCheckpoint(99, { from: gameAdmin }), 'rare win is invalid')
    //   await catchRevertWithReason(luckyNumberInstance.setRareWinCheckpoint(9501, { from: gameAdmin }), 'rare win is invalid')
    // })
    // it('3. success' , async () => {
    //   const rareWinCheckpoint = 100
    //   await luckyNumberInstance.setRareWinCheckpoint(rareWinCheckpoint, { from: gameAdmin })
    //   const contractHighRollCheckpoint = await luckyNumberInstance.rareWinCheckpoint()
    //   contractHighRollCheckpoint.should.be.a.bignumber.that.equal(`${rareWinCheckpoint}`)
    //
    // })
    // it('4. should fire event' , async () => {
    //   const response = await luckyNumberInstance.setRareWinCheckpoint(100, { from: gameAdmin })
    //   util.listenEvent(response, 'RareWinCheckpointSet')
    // })
  })

  describe('VIII. Admin getAdminBalance', () => {
    it('1. fail cause by not admin' , async () => {
      await catchRevertWithReason(luckyNumberInstance.getAdminBalance({ from: user1 }), 'onlyMainAdmin')
    })
    it('3. success' , async () => {
      const adminBalance = await luckyNumberInstance.getAdminBalance({ from: gameAdmin })
      adminBalance.should.be.a.bignumber
    })
  })

  describe('VIII. Admin transferOwnership', () => {
    it('1. fail cause by not admin' , async () => {
      await catchRevertWithReason(luckyNumberInstance.updateGameAdmin(user1, { from: user1 }), 'onlyAdmin')
    })
    it('3. success from gameAdmin' , async () => {
      const newGameAdmin = user1
      await luckyNumberInstance.updateGameAdmin(newGameAdmin, { from: gameAdmin })
      const adminBalance = await luckyNumberInstance.getAdminBalance({ from: newGameAdmin })
      adminBalance.should.be.a.bignumber
    })
    it('3. success from mainAdmin' , async () => {
      const newGameAdmin = user1
      await luckyNumberInstance.updateGameAdmin(newGameAdmin, { from: mainAdmin })
      const adminBalance = await luckyNumberInstance.getAdminBalance({ from: newGameAdmin })
      adminBalance.should.be.a.bignumber
    })
  })
})
