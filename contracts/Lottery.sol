pragma solidity 0.4.25;

import "./libs/dice/Auth.sol";
import "./libs/zeppelin/math/SafeMath.sol";
import "./libs/zeppelin/token/ERC20/IERC20.sol";

contract Lottery is Auth {
    using SafeMath for uint;

    string private seed;
    uint public lotteryAmount;
    uint public lotteryPriceBQT;
    IERC20 public bqToken = IERC20(0xb8Fbe1e9d5Eb0be464EdfDea54999f7349b8282B);
    uint8 public currentTicket = 0;
    address[] public tickets;
    uint8 public totalRound = 1;

    struct User {
        address addressUser;
        uint lottery;
    }
    mapping(address => User) public users;

    event AmountTicketSet(uint _number);
    event PriceTicketSet(uint _price);
    event RunLottery(uint indexed _totalRound);
    event BuyTicket(uint8 indexed _round, address indexed _user, uint numLottery);
    event Award(
        uint8 _totalRound,
        uint _currentFund,
        uint _currentETH,
        address _firstAward,
        address _secondAwardNo1,
        address _secondAwardNo2,
        address _thirdAwardNo1,
        address _thirdAwardNo2
    );
    modifier validNumber(uint _number) {
        require(_number > 0, 'Number must be > 0');
        _;
    }

    // PUBLIC FUNCTION
    constructor(string _seed, uint8 _lotteryAmount, uint _lotteryPrice) Auth(msg.sender, msg.sender) public {
        lotteryPriceBQT = (_lotteryPrice == 0) ? 10e18 : _lotteryPrice;
        lotteryAmount = (_lotteryAmount == 0) ? 5 : _lotteryAmount;
        seed = _seed;
    }

    function setAmountTicket(uint _number) onlyMainAdmin validNumber(_number) public {
        lotteryAmount = _number;
        emit AmountTicketSet(_number);
    }

    function setPriceTicket(uint _price) onlyMainAdmin validNumber(_price) public {
        lotteryPriceBQT = _price;
        emit PriceTicketSet(_price);
    }

    function buyTicketETH() public payable returns (bool) {
        require(msg.value == lotteryPriceBQT / (bqToken.getRateBQT()), 'Transfer the right amount');
        buySuccessfully();
        return true;
    }

    function buyTicketBQT() public returns(bool) {
        require(bqToken.allowance(msg.sender, address(this)) >= lotteryPriceBQT, "You must call approve() first");
        require(bqToken.transferFrom(msg.sender, address(this), lotteryPriceBQT), "Transfer token failed");
        buySuccessfully();
        return true;
    }

    function award() onlyMainAdmin public {
        require(currentTicket == lotteryAmount, "Not enough ticket to lottery!");
        uint currentFund = bqToken.balanceOf(address(this));
        uint currentETH = address(this).balance;
        address firstAward = pickerWinner(address(0x0));
        uint prizeBQT = 0;
        uint prizeETH = 0;
        if (firstAward != address(0x0)) {
            prizeBQT = currentFund.mul(2).div(5);
            prizeETH = currentETH.mul(2).div(5);
            require(bqToken.transfer(firstAward, prizeBQT), "Transfer token to 1st winner failed");
            firstAward.transfer(prizeETH);
        }
        address secondAwardNo1 = pickerWinner(firstAward);
        if (secondAwardNo1 != address(0x0) && secondAwardNo1 != firstAward) {
            prizeBQT = currentFund.mul(3).div(20);
            prizeETH = currentETH.mul(3).div(20);
            require(bqToken.transfer(secondAwardNo1, prizeBQT), "Transfer token to 2st winner failed");
            secondAwardNo1.transfer(prizeETH);
        }
        address secondAwardNo2 = pickerWinner(secondAwardNo1);
        if (secondAwardNo2 != address(0x0) && secondAwardNo2 != firstAward && secondAwardNo2 != secondAwardNo1) {
            require(bqToken.transfer(secondAwardNo2, prizeBQT), "Transfer token to 2st winner failed");
            secondAwardNo2.transfer(prizeETH);
        }
        address thirdAwardNo1 = pickerWinner(secondAwardNo2);
        if (thirdAwardNo1 != address(0x0) && thirdAwardNo1 != firstAward && thirdAwardNo1 != secondAwardNo1 && thirdAwardNo1 != secondAwardNo2) {
            prizeBQT = currentFund.div(10);
            prizeETH = currentETH.div(10);
            require(bqToken.transfer(thirdAwardNo1, prizeBQT), "Transfer token to 3st winner failed");
            thirdAwardNo1.transfer(prizeETH);
        }
        address thirdAwardNo2 = pickerWinner(thirdAwardNo1);
        if (thirdAwardNo2 != address(0x0) && thirdAwardNo2 != firstAward && thirdAwardNo2 != secondAwardNo1 && thirdAwardNo2 != secondAwardNo2 && thirdAwardNo2 != thirdAwardNo1) {
            require(bqToken.transfer(thirdAwardNo2, prizeBQT), "Transfer token to 3st winner failed");
            thirdAwardNo2.transfer(prizeETH);
        }

        emit Award(
            totalRound,
            currentFund,
            currentETH,
            firstAward,
            secondAwardNo1,
            secondAwardNo2,
            thirdAwardNo1,
            thirdAwardNo2
        );
        totalRound += 1;
        currentTicket = 0;
    }

    function getAmountTicketCurent() public returns (uint) {
        return tickets.length;
    }

    // PRIVATE FUNCTION

    function genRandomNumber(address _seed) private view returns (uint) {
        uint amount = lotteryAmount.add(lotteryAmount.mul(10).div(100));
        return uint(uint(
            keccak256(
                abi.encodePacked(
                    block.timestamp,
                    block.difficulty,
                    msg.sender,
                    now,
                    _seed,
                    seed
                )
            )
        ) % amount) * 5 % amount;
    }

    function checkLottery() private view returns (bool) {
        return tickets.length == lotteryAmount;
    }

    function buySuccessfully() private {
        User storage user = users[msg.sender];
        user.addressUser = msg.sender;
        user.lottery = tickets.length;
        addTicket();
        if (checkLottery()) {
            emit RunLottery(totalRound);
        }
        emit BuyTicket(totalRound, msg.sender, user.lottery);
    }

    function addTicket() private {
        tickets.push(msg.sender);
    }

    function pickerWinner(address _seed) private view returns (address){
        uint randomIndex = genRandomNumber(_seed);
        return randomIndex < currentTicket ? tickets[randomIndex] : address(0x0);
    }

    function resetGame() private {
        delete tickets;
    }
}